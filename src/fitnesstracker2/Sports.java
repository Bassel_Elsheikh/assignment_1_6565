/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitnesstracker2;

/**
 *
 * @author beso_
 */
public class Sports implements Comparable<Sports>{

    int calories;
    int id;
    double heartrate;
    static int totalcal;
    static double totalrate=80;

    public  void addrun(int time) {
        this.calories += 5;
        
        this.heartrate += (totalrate * time * 0.003);
        totalrate += (totalrate * time * 0.003);
        totalcal += (time * 5);

    }

    public  void addswim(int time) {
        this.calories += 4;
        
        this.heartrate += (totalrate * time * 0.002);
        totalrate += (totalrate * time * 0.002);
        
        totalcal += (time * 4);

    }

    public  void addkick(int time) {
        this.calories += 3;
        
        this.heartrate += (totalrate * time * 0.005);
        totalrate += (totalrate * time * 0.005);
        
        totalcal += (time * 3);

    }

    public  void addstr(int time) {
        this.calories += 5;
        
        this.heartrate += (totalrate * time * 0.006);
        totalrate += (totalrate * time * 0.006);
        
        totalcal += (time * 5);

    }

    public Sports(int calories, double heartrate, int id) {
        this.calories = calories;
        this.heartrate = heartrate;
        this.id=id;
    }

    @Override
    public int compareTo(Sports o) {
         int comp;
                 comp = Integer.compare(calories,o.calories);
                 if (comp==0)
                 {
                    comp=Double.compare(heartrate, o.heartrate);
                     
                 }
               return comp;      
                
                     
                 
    }

    @Override
    public String toString() {
        return "calories burnt: " + calories + "\n" + "heartrate increase: " + heartrate + "\n";
    }

}

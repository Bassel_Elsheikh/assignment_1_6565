/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitnesstracker2;

/**
 *
 * @author beso_
 */
public class GeneralData {

    static int runcal;
    static int swimcal;
    static int kickcal;
    static int strcal;
    static double runrate;
    static double swimrate;
    static double kickrate;
    static double strrate;
    
    public  static void addrun(int time) {
        runcal += (5*time);
        
        runrate += (Sports.totalrate * time * 0.003);
        Sports.totalrate += (Sports.totalrate * time * 0.003);
        Sports.totalcal += (time * 5);

    }

    public  static void addswim(int time) {
        swimcal += (4*time);
        
        swimrate += (Sports.totalrate * time * 0.002);
        Sports.totalrate += (Sports.totalrate * time * 0.002);
        Sports.totalcal += (time * 4);

    }

    public  static void addkick(int time) {
        kickcal += (3*time);
        
        kickrate += (Sports.totalrate * time * 0.005);
        Sports.totalrate += (Sports.totalrate * time * 0.005);
        Sports.totalcal += (time * 3);

    }

    public  static void addstr(int time) {
        strcal += (5*time);
        
        strrate += (Sports.totalrate * time * 0.006);
        Sports.totalrate += (Sports.totalrate * time * 0.006);
        Sports.totalcal += (time * 5);

    }
    
   
    
    
    
}
